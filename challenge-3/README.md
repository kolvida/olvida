# Instructions

This one will get you fiddling with display, position, widths, margins and padding!

### Hints:

- Flex can help. Remember that a div's display property is inherited. 
- You can recycle your code. Work smart.


